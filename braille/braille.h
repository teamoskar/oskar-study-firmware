/*
  Copyright 2018 Johannes Strelka-Petz
*/
/*
    This file is part of Oskar.

    Oskar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oskar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef braille_h
#define braille_h

// english layout usb scancodes https://web.stanford.edu/class/cs140/projects/pintos/specs/kbd/scancodes-9.html
// here is the german layout implemented
#define d1 1
#define d2 2
#define d3 4
#define d4 8
#define d5 16
#define d6 32
#define d7 64
#define d8 128
#define d9 256
#define d10 512
#define d11 1024
#define d12 2048

#define Key_A		4
#define Key_B		5
#define Key_C		6
#define Key_D		7
#define Key_E		8
#define Key_F		9
#define Key_G		10
#define Key_H		11
#define Key_I		12
#define Key_J		13
#define Key_K		14
#define Key_L		15
#define Key_M		16
#define Key_N		17
#define Key_O		18
#define Key_P		19
#define Key_Q		20
#define Key_R		21
#define Key_S		22
#define Key_T		23
#define Key_U		24
#define Key_V		25
#define Key_W		26
#define Key_X		27
#define Key_Y		29
#define Key_Z		28

#define Key_1 30
#define Key_2 31
#define Key_3 32
#define Key_4 33
#define Key_5 34
#define Key_6 35
#define Key_7 36
#define Key_8 37
#define Key_9 38
#define Key_0 39
#define Key_ENTER 40
#define GREF_ENTER d1+d3+d4+d7+d8 // 0xcd 205 dots-13478
#define Key_ESC 41
#define Key_BACKSPACE 42
#define Key_TAB 43
#define Key_SPACEBAR 44
#define Key_MINUS 0x38
#define GREF_GEDANKENSTRICH 36
#define Key_BACKSLASH 45
#define GREF_BACKSLASH 51
#define Key_DOT 55
#define Key_COMMA 54
#define GREF_SLASH 30
#define Key_SLASH Key_7
#define Key_PAUSE 72
#define Key_INSERT 73
#define Key_HOME 74
#define Key_PAGEUP 75
#define Key_DELETE 76
#define Key_END 77
#define Key_PAGEDOWN 78
#define Key_ARROW_RIGHT 79
// #define GREF_ARROW_RIGHT 245 // 0xf5 dots-135678
#define GREF_ARROW_RIGHT d12 // thumb
#define Key_WORD_RIGHT Key_ARROW_RIGHT
#define GREF_WORD_RIGHT 58
#define Key_ARROW_LEFT 80
// #define GREF_ARROW_LEFT 247 // 0xf7 dots-1235678
#define GREF_ARROW_LEFT d11 // thumb
#define Key_WORD_LEFT Key_ARROW_LEFT
#define GREF_WORD_LEFT 23
#define Key_ARROW_DOWN 81
// #define GREF_ARROW_DOWN 253 // 0xfd dots-1345678
#define GREF_ARROW_DOWN d10 // thumb
#define Key_ARROW_UP 82
//#define GREF_ARROW_UP 237 // 0xed dots-134678
#define GREF_ARROW_UP d9 // thumb
#define Key_BANG Key_1
#define GREF_BANG 22
#define Key_th 4
#define Key_ue 47
#define Key_that 4
#define Key_the 4
#define Key_ae 52
#define Key_of 4
#define Key_oe 51
#define Key_quotationmark Key_2
#define GREF_quotationmark 10
#define Key_and 4
#define Key_sz 45
#define Key_to Key_sz
#define Key_quote1 46//'
#define GREF_quote1 30//'
#define Key_quote2 Key_quote1//`
#define GREF_quote2 51//`
#define Key_questionmark Key_sz 
#define GREF_questionmark 34
#define Key_PARAGRAPH Key_3
#define GREF_PARAGRAPH 44
#define Key_num 0x31
#define Key_singlequote Key_num	//Apostroph
#define GREF_singlequote 32
#define GREF_apostroph 32
#define Key_WLeft 0
#define Key_WRight 0
#define Key_SYMB 0
#define Key_123abc 0
#define Key_percent Key_5
#define GREF_percent 63
#define Key_dollar Key_4
#define GREF_dollar 40
#define Key_and2 Key_6
#define GREF_and2 47
#define Key_semicolon Key_COMMA
#define GREF_semicolon 6
#define Key_colon Key_DOT
#define GREF_colon 18

//multi character
#define Key_st 0
#define GREF_st 62
#define Key_au 0
#define GREF_au 33
#define Key_eu 0
#define GREF_eu 35
#define Key_ei 0
#define GREF_ei 41
#define Key_ch 0
#define GREF_ch 57
#define Key_sch 0
#define GREF_sch 49
#define Key_ein 0
#define GREF_ein 43
#define Key_er 0
#define GREF_er 59
#define Key_be 0
#define GREF_be 6
#define Key_al 0
#define GREF_al 18
#define Key_un 0
#define GREF_un 50
#define Key_or 0
#define GREF_or 34
#define Key_an 0
#define GREF_an 22
#define Key_eh 0
#define GREF_eh 54
#define Key_tt 0
#define GREF_tt 38
#define Key_in 0
#define GREF_in 20
#define Key_ar 0
#define GREF_ar 52
#define Key_ig 0
#define GREF_ig 24
#define Key_aeu 0
#define GREF_aeu 12
#define Key_ie 0
#define GREF_ie 44
#define Key_lich 0
#define GREF_lich 56
#define Key_ach 0
#define GREF_ach 48

#define GREF_7 64
#define GREF_8 128
#define GREF_9 256
#define GREF_10 512
#define GREF_11 1024
#define GREF_12 2048
#define MOD_CTRL_LEFT		0x01
#define MOD_SHIFT_LEFT		0x02
#define MOD_ALT_LEFT		0x04
#define MOD_GUI_LEFT		0x08
#define MOD_CTRL_RIGHT		0x10
#define MOD_SHIFT_RIGHT		0x20
#define MOD_ALT_RIGHT		0x40
#define MOD_GUI_RIGHT		0x80

#define Key_at Key_Q
#define GREF_at 49
#define Key_half 39 //? not working?

#define Key_plus 0x30
#define Key_underscore Key_MINUS
#define GREF_underscore 17
#define Key_equal Key_0
#define GREF_equal 19
#define Key_top 53 
#define GREF_top 13
#define GREF_top_symb 29
#define Key_degree Key_top
#define GREF_degree 43
#define Key_star 85
#define GREF_star 20 //16+4 0x14 dots-35

#define Key_euro Key_E
#define GREF_euro 22
#define Key_pound 181
#define Key_leftbraket Key_8
#define GREF_leftbraket 40
#define Key_leftsquarebraket Key_8 
#define GREF_leftsquarebraket 54 //32+16+4+2 0x36 dots-2356
#define Key_leftbraket2 Key_7
#define GREF_leftbraket2 44
#define Key_rightbraket Key_9
#define GREF_rightbraket 5
#define Key_rightsquarebraket Key_9
#define GREF_rightsquarebraket 13
#define Key_more Key_less
#define GREF_more 21
#define Key_rightbraket2 Key_0
#define GREF_rightbraket2 37
#define Key_mikro Key_M
#define GREF_mikro 53
#define Key_less 0x64
#define Key_pipe Key_less
#define GREF_pipe 12
#define Key_tilde Key_plus
#define GREF_tilde 33
#define Key_empty 0

#define Key_quote Key_2
#define GREF_quote 36
#define Key_leftquote 53
#define GREF_leftquote 51
#define Key_rightquote 0x2e
#define GREF_rightquote 30

#define GREF_SHIFT 64
#define GREF_SYMB 64+1
#define GREF_123ABC 64+2
#define GREF_CTRL 64+3
#define GREF_MOD 64+4

#endif
