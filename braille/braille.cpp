/*
  Copyright 2018 Johannes Strelka-Petz, johannes_at_strelka.at
*/
/*
    This file is part of Oskar.

    Oskar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oskar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Gkos.h>
#include <scancode_keyboard.h>
#include <braille.h>

boolean debug=false;
byte gKey = 0; 
unsigned int chord = 0;
byte mod = 0;
byte multisign = 0;
int gRefmod = 0;
boolean g123abc = false;

// german braille layout
// https://en.wikipedia.org/wiki/German_Braille

const static byte _gUsageID[]={
  0,
  Key_A, //1 0x01 dots-1
  Key_COMMA, //2 0x02 dots-2
  Key_B, //3 1+2 0x03 dots-12
  Key_DOT, //4 0x04 dots-3  
  Key_K, //5 4+1 0x05 dots-13
  //  Key_be, //6 4+2 0x06 dots-23
  Key_semicolon, //6 4+2 0x06 dots-23
  Key_L, //7 4+2+1 0x07 dots-123
  Key_DOT, //8 
  Key_C, //9 8+1 0x09 dots-14
  Key_I, //10 8+2 0x0a dots-24
  Key_F, //11 8+2+1 0x0b dots-124
  Key_aeu, //12 8+4 0x0c dots-34
  Key_M, //13 8+4+1 0x0d dots-134
  Key_S, //14 8+4+2 0x0e dots-234
  Key_P, //15 8+4+2+1 0x0f dots-1234
  0, //16  0x10
  Key_E, //17 16+1 0x11 dots-15
  //  Key_al, //18 16+2 0x12 dots-25
  Key_colon, //18 16+2 0x12 dots-25
  Key_H, //19 16+2+1 0x13 dots-125
  Key_in, //20 16+4 0x14 dots-35
  Key_O, //21 16+4+1 0x15 dots-135
  //  Key_an, //22 16+4+2 0x16 dots-235
  Key_BANG, //22 16+4+2 0x16 dots-235  
  Key_R, //23 16+4+2+1 0x17 dots-1235
  Key_ig, //24 16+8 0x18 dots-45
  Key_D, //25 16+8+1 0x19 dots-156
  Key_J, //26 16+8+2 0x1a dots-245
  Key_G, //27 16+8+2+1 0x1b dots-1245
  Key_ae, //28 16+8+4 0x1c dots-345
  Key_N, //29 16+8+4+1 0x1d dots-1345
  Key_T, //30 16+8+4+2 0x1e dots-2345
  Key_Q, //31 16+8+4+2+1 0x1f dots-12345
  Key_singlequote, //32 0x20 dots-6
  Key_au, //33 32+1 0x21 dots-16
  //  Key_or, //34 32+2 0x22 dots-26
  Key_questionmark, //34 32+2 0x22 dots-26
  Key_eu, //35 32+2+1 0x23 dots-126
  Key_MINUS, //36 32+4 0x24 dots-36
  Key_U, //37 32+4+1 0x25 dots-136
  Key_tt, //38 32+4+2 0x26 dots-236
  Key_V, //39 32+4+2+1 0x27 dots-1236
  Key_dollar, //40 32+8 0x28 dots-46
  Key_ei, //41 32+8+1 0x29 dots-146
  Key_oe, //42 32+8+2 0x2a dots-246
  Key_ein, //43 32+8+2+1 0x2b dots-1246
  Key_ie, //44 32+8+4 0x2c dots-346
  Key_X, //45 32+8+4+1 0x2d dots-1346
  Key_sz, //46 32+8+4+2 0x2e dots-2346
  Key_and2, //47 32+8+4+2+1 0x2f dots-12346
  Key_ach, //48 32+16 0x30 dots-56
  Key_sch, //49 32+16+1 0x31 dots-156
  Key_un, //50 32+16+2 0x32 dots-256
  Key_ue, //51 32+16+2+1 0x33 dots-1256
  Key_ar, //52 32+16+4 0x34 dots-356
  Key_Z, //53 32+16+4+1 0x35 dots-1356
  Key_eh, //54 32+16+4+2 0x36 dots-2356
  Key_sz, //55 32+16+4+2+1 0x37 dots-12356
  Key_lich, //56 32+16+8 0x38 dots-456
  Key_ch, //57 32+16+8+1 0x39 dots-1456
  Key_W, //58 32+16+8+2 0x3a dots-2456
  Key_er, //59 32+16+8+2+1 0x3b dots-12456
  Key_num, //60 32+16+8+4 0x3c dots-3456
  Key_Y, //61 32+16+8+4+1 0x3d dots-13456
  Key_st, //62 32+16+8+4+2 0x3e dots-23456
  Key_percent //63 32+16+8+4+2+1 0x3f dots-123456
};

// Initialize Gkos library with pin numbers for keys A to L.
//        A  B  C   D   E   F   G  H   I  J  K  L
//        1  2  3   4   5   6   7  8   9  10 11 12
//                 A0  A1  A2     A3
Gkos gkos(6, 5, 4, 18, 19, 20, 3, 21, 10, 16, 7, 8);

void setup() {
  if (debug){Serial.begin(9600);}
  Scancode_Keyboard.begin();
}

void loop() {
  chord = gkos.chord();
  if (chord != 0) {
    if (multisign!=0){
      switch (multisign){
      case GREF_apostroph:
	switch (chord){
	case GREF_GEDANKENSTRICH:
	  Scancode_Keyboard.write(Key_BACKSPACE);
	  Scancode_Keyboard.write(Key_MINUS);
	  break;
	case GREF_star:
	  Scancode_Keyboard.write(Key_BACKSPACE);	  
	  Scancode_Keyboard.write(Key_star);
	  break;
	case GREF_leftsquarebraket:
	  Scancode_Keyboard.write(Key_BACKSPACE);	  
	  mod=MOD_ALT_RIGHT;
	  Scancode_Keyboard.press(Key_leftsquarebraket,mod);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  mod=0;
	  break;
	}
	multisign=0;
      }
    }
    else {
      if (debug){Serial.println(chord);}
      // upper case letters
      if (chord > GREF_7 && chord < GREF_8){
	gKey = (byte)_gUsageID[chord-GREF_7];
	mod=MOD_SHIFT_LEFT;
      }
      // thumb space chords
      else if (chord > d9+d10 && chord < d11){
	switch (chord){
	case d1+d9+d10: // a+space, move to previous item
	  Scancode_Keyboard.press(Key_ARROW_LEFT,MOD_ALT_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d2+d9+d10: // +space, move to item above
	  Scancode_Keyboard.press(Key_ARROW_UP,MOD_ALT_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d1+d2+d9+d10: // b+space, back
	  Scancode_Keyboard.press(Key_DELETE,MOD_ALT_LEFT | MOD_CTRL_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d1+d2+d3+d9+d10: // l+space, move to first item
	  Scancode_Keyboard.press(Key_ARROW_LEFT,MOD_ALT_LEFT | MOD_CTRL_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d4+d9+d10: // +space, move to next item
	  Scancode_Keyboard.press(Key_ARROW_RIGHT,MOD_ALT_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d1+d2+d3+d4+d9+d10: // p+space, pause or resume talkback
	  Scancode_Keyboard.press(Key_Z,MOD_ALT_LEFT | MOD_CTRL_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d5+d9+d10: // +space, move to item below
	  Scancode_Keyboard.press(Key_ARROW_DOWN,MOD_ALT_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d1+d2+d5+d9+d10: // h+space, home
	  Scancode_Keyboard.press(Key_H,MOD_ALT_LEFT | MOD_CTRL_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d1+d2+d3+d5+d9+d10: // r+space, read from next item
	  Scancode_Keyboard.press(Key_ENTER,MOD_ALT_LEFT | MOD_CTRL_LEFT | MOD_SHIFT_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d1+d4+d5+d9+d10: // d+space, backspace
	  Scancode_Keyboard.write(Key_BACKSPACE);
	  break;
	case d3+d6+d9+d10: // :+space, doubletap, activate
	  Scancode_Keyboard.press(Key_ENTER,MOD_ALT_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d2+d4+d5+d6+d9+d10: // w+space, read from top
	  Scancode_Keyboard.press(Key_ENTER,MOD_ALT_LEFT | MOD_CTRL_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  break;
	case d4+d5+d6+d9+d10: // +space, move to last item
	  Scancode_Keyboard.press(Key_ARROW_RIGHT,MOD_ALT_LEFT | MOD_CTRL_LEFT);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	}
      }
      // default lookup
      else {gKey = (byte)_gUsageID[chord];}
      //modifier for special characters
      switch (chord){
      case GREF_and2:
      case GREF_percent:
      case GREF_dollar:
      case GREF_singlequote:
      case GREF_semicolon:
      case GREF_colon:
      case GREF_questionmark:
      case GREF_BANG:
      case GREF_PARAGRAPH:      
	mod=MOD_SHIFT_LEFT;
      }
      switch (chord){
      case GREF_7:
	Scancode_Keyboard.write(Key_BACKSPACE);
	break;
      case GREF_8:
	Scancode_Keyboard.write(Key_SPACEBAR);
	break;
      case d9+d10: // thumb center pushbutton
	Scancode_Keyboard.write(Key_SPACEBAR);
	break;	
      case GREF_st:
	Scancode_Keyboard.write(Key_S);
	Scancode_Keyboard.write(Key_T);
	break;
      case GREF_au:
	Scancode_Keyboard.write(Key_A);
	Scancode_Keyboard.write(Key_U);
	break;
      case GREF_eu:
	Scancode_Keyboard.write(Key_E);
	Scancode_Keyboard.write(Key_U);
	break;
      case GREF_ei:
	Scancode_Keyboard.write(Key_E);
	Scancode_Keyboard.write(Key_I);
	break;
      case GREF_ch:
	Scancode_Keyboard.write(Key_C);
	Scancode_Keyboard.write(Key_H);
	break;
      case GREF_sch:
	Scancode_Keyboard.write(Key_S);
	Scancode_Keyboard.write(Key_C);
	Scancode_Keyboard.write(Key_H);
	break;
      case GREF_aeu:
	Scancode_Keyboard.write(Key_ae);
	Scancode_Keyboard.write(Key_U);
	break;
      case GREF_ie:
	Scancode_Keyboard.write(Key_I);
	Scancode_Keyboard.write(Key_E);
	break;
      case GREF_ENTER:
	Scancode_Keyboard.write(Key_ENTER);
	break;
      case GREF_ARROW_UP:
	Scancode_Keyboard.write(Key_ARROW_UP);
	break;
      case GREF_ARROW_RIGHT:
	Scancode_Keyboard.write(Key_ARROW_RIGHT);
	break;
      case GREF_ARROW_LEFT:
	Scancode_Keyboard.write(Key_ARROW_LEFT);
	break;
      case GREF_ARROW_DOWN:
	Scancode_Keyboard.write(Key_ARROW_DOWN);
	break;
      default:
	if (mod == 0){
	  if (debug){
	    Serial.println(chord);
	    Serial.println(gKey);
	    Scancode_Keyboard.press(gKey,mod);
	    Scancode_Keyboard.release(gKey,mod);
	  }
	  else {
	    Scancode_Keyboard.write(gKey);
	  }
	}
	else {
	  if (debug){
	    Serial.print("mod: ");
	    Serial.println(mod);
	    Serial.print("chord: ");
	    Serial.println(chord);
	    Serial.print("gKey: ");
	    Serial.println(gKey);
	  }
	  Scancode_Keyboard.press(gKey,mod);
	  delay(100);
	  Scancode_Keyboard.releaseAll();
	  mod=0;
	}
      }
      switch (chord){
      case GREF_apostroph:
	multisign=GREF_apostroph;
      }
    }
  }
  delay(1);
}

