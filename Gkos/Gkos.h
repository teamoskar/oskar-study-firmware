/*
  Gkos.h - Library for reading a GKOS keyboard.
  Created by Seppo Tiainen, March 28, 2010.
  Released into the public domain.
*/
/*
  Modified work Copyright 2018 Johannes Strelka-Petz
*/
/*
    This file is part of Oskar.

    Oskar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oskar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef Gkos_h
#define Gkos_h

/*Replace #include "WProgram.h" with the following code to make compatible with
latest Arduino IDE.*/
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
  #else
  #include "WProgram.h"
  #endif
//#include "Wire.h"
//#include "wm_crypto.h"
/*Replace #include "WProgram.h" with the above code to make compatible with
latest Arduino IDE.*/
//#include "WProgram.h"

//#include <string.h>
//#include <inttypes.h>
//#include "Print.h"

// Declaring functions and variables //////////////////////
class Gkos
{
  public:
    // Initialize I/O pin numbers for GKOS keys A to L
    Gkos(uint8_t APin, uint8_t BPin, uint8_t CPin, uint8_t DPin, uint8_t EPin, uint8_t FPin, uint8_t GPin, uint8_t HPin, uint8_t IPin, uint8_t JPin, uint8_t KPin, uint8_t LPin);
    char* entry();  // gkos.entry(), returns the character
    int reference(); // gkos.reference, returns gRef
    unsigned int chord(); //gkos.chord, returns gChord
  private:
    bool gScanKeys();
    int _gRef;             // the GKOS Reference number (also for Shift/CAPS, Symb, 123abc)
    uint8_t _key_pins[12];  // Inputs of keys
    uint8_t _key_down[12];  // Status of keys (1 = pressed)
    uint8_t _shifts;    // 1=Shift, 2=Caps, 4=Nums, 8=Symb 

    static char* gOutput[10]; // for storing character

    int _time;
    int _previousTime;
    unsigned int _gChord; // GKOS final chord value (0...63) for the character
    unsigned int _firstChord; // First chord in a Chordon, if any 
    uint8_t _firstCounter; // Detect first chord in a Chordon
    unsigned int _previousChord; // to compare chord change
    unsigned int _presentChord; // the chord of the keypad at present
    uint8_t _autoRepeat;   // Repeat the character if pressed long (0 or 1)
    uint8_t _autoCounter;  // Typamatic delay counter (n x 10 ms)
    bool _gNew; // a new character is expected soon because new keys were pressed

};

#endif


