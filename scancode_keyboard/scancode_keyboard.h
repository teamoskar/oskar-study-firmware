/*
  Copyright 2018 Johannes Strelka-Petz
*/
/*
    This file is part of Oskar.

    Oskar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oskar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef scancode_keyboard_h
#define scancode_keyboard_h

//#include "USBAPI.h"

class Scancode_Keyboard_ //: public Print
{
private:
	KeyReport _keyReport;
	void sendReport(KeyReport* keys);
public:
	Scancode_Keyboard_(void);
	void begin(void);
	void end(void);
	virtual size_t write(uint8_t k);
	virtual size_t press(uint8_t k, uint8_t mod);
	virtual size_t release(uint8_t k, uint8_t mod);
	virtual void releaseAll(void);
};
extern Scancode_Keyboard_ Scancode_Keyboard;

#endif
