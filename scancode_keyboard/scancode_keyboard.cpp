/*
  Copyright 2018 Johannes Strelka-Petz
*/
/*
    This file is part of Oskar.

    Oskar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Oskar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Platform.h>
#include <USBAPI.h>
#include <USBDesc.h>
#include <scancode_keyboard.h>

Scancode_Keyboard_ Scancode_Keyboard;

Scancode_Keyboard_::Scancode_Keyboard_(void) 
{
}

void Scancode_Keyboard_::begin(void) 
{
}

void Scancode_Keyboard_::end(void) 
{
}

void Scancode_Keyboard_::sendReport(KeyReport* keys)
{
	HID_SendReport(2,keys,sizeof(KeyReport));
}

// press() adds the specified key (printing, non-printing, or modifier)
// to the persistent key report and sends the report.  Because of the way 
// USB HID works, the host acts like the key remains pressed until we 
// call release(), releaseAll(), or otherwise clear the report and resend.
size_t Scancode_Keyboard_::press(uint8_t k, uint8_t mod) 
{
	uint8_t i;
	_keyReport.modifiers |= mod;
	// Add k to the key report only if it's not already present
	// and if there is an empty slot.
	if (_keyReport.keys[0] != k && _keyReport.keys[1] != k && 
		_keyReport.keys[2] != k && _keyReport.keys[3] != k &&
		_keyReport.keys[4] != k && _keyReport.keys[5] != k) {
		
		for (i=0; i<6; i++) {
			if (_keyReport.keys[i] == 0x00) {
				_keyReport.keys[i] = k;
				break;
			}
		}
		if (i == 6) {
		  //setWriteError();
		  return 0;
		}	
	}
	sendReport(&_keyReport);
	return 1;
}

// release() takes the specified key out of the persistent key report and
// sends the report.  This tells the OS the key is no longer pressed and that
// it shouldn't be repeated any more.
size_t Scancode_Keyboard_::release(uint8_t k, uint8_t mod) 
{
	uint8_t i;
	_keyReport.modifiers &= ~(mod);
	// Test the key report to see if k is present.  Clear it if it exists.
	// Check all positions in case the key is present more than once (which it shouldn't be)
	for (i=0; i<6; i++) {
		if (0 != k && _keyReport.keys[i] == k) {
			_keyReport.keys[i] = 0x00;
		}
	}

	sendReport(&_keyReport);
	return 1;
}

void Scancode_Keyboard_::releaseAll(void)
{
	_keyReport.keys[0] = 0;
	_keyReport.keys[1] = 0;	
	_keyReport.keys[2] = 0;
	_keyReport.keys[3] = 0;	
	_keyReport.keys[4] = 0;
	_keyReport.keys[5] = 0;	
	_keyReport.modifiers = 0;
	sendReport(&_keyReport);
}

size_t Scancode_Keyboard_::write(uint8_t k)
{	
  uint8_t p = press(k,0);		// Keydown
  uint8_t r = release(k,0);		// Keyup
  return (p);					// just return the result of press() since release() almost always returns 1
}
