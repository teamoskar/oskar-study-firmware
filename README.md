# Oskar
open source key arrangement
a mobile braille keyboard
connected over USB.

## Build
build with Arduino 1.0.5 and Arduino-Makefile
```
cd braille
make upload
```
## Contact
Johannes Strelka-Petz, johannes_at_strelka.at